﻿using Neo4j.Driver.V1;
using System;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    interface ITransactionManager : IDisposable
    {
        ITransaction GetTransaction();

        /// <summary>
        /// Commit a transaction
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        Task CommitAsync(ITransaction transaction);

        /// <summary>
        /// Rollback a transaction
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        Task RollbackAsync(ITransaction transaction);
    }
}
