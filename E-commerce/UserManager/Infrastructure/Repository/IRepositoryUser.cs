﻿using ApplicationCore.Domain;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public interface IRepositoryUser
    {
        Task<bool> InsertAsync(User user);
    }
}
