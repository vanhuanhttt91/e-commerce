﻿using ApplicationCore.Domain;
using Neo4jClient;
using System;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class RepositoryUser : IRepositoryUser
    {
        private readonly IBoltGraphClient _graphClient;

        public RepositoryUser(IBoltGraphClient graphClient)
        {
            _graphClient = graphClient;
        }
        public async Task<bool> InsertAsync(User user)
        {
            try
            {
                Guid guid = Guid.NewGuid();
                var cypherQuery = _graphClient.Cypher.Write.Create($"(U: {Nodes.User} {{ Id: '{user.Id }', Name: '{user.Name}'}})");
                await cypherQuery.ExecuteWithoutResultsAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

    }
}
