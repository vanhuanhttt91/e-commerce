using ApplicationCore.Domain;
using ApplicationCore.Service;
using AutoMapper;
using Infrastructure.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Neo4j.Driver.V1;
using Neo4jClient;
using System.Reflection;

namespace UserManager
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.Configure<Neo4jSetting>(Configuration.GetSection("Neo4jSetting"));
            services.AddAutoMapper(Assembly.GetAssembly(typeof(MappingConfiguration)));
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IRepositoryUser, RepositoryUser>();
            // RegisterJWTAuthentication(services);
            services.AddMvc();
            RegisterNeo4jDriver(services);
            RegisterGraphClient(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
        private IServiceCollection RegisterNeo4jDriver(IServiceCollection services)
        {
            services.AddSingleton(typeof(IDriver), resolver =>
            {
                var neo4jSetting = Configuration.GetSection("Neo4jSetting").Get<Neo4jSetting>();
                var authToken = AuthTokens.Basic(neo4jSetting.UserName, neo4jSetting.Password);
                var config = new Config();
                var driver = GraphDatabase.Driver(neo4jSetting.BoltUrl, authToken, config);
                return driver;
            });

            return services;
        }

        private IServiceCollection RegisterGraphClient(IServiceCollection services)
        {
            services.AddSingleton(typeof(IBoltGraphClient), resolver =>
            {
                var client = new BoltGraphClient(services.BuildServiceProvider().GetService<IDriver>());

                if (!client.IsConnected)
                {
                    client.Connect();
                }

                return client;
            });

            return services;
        }

    }
}
