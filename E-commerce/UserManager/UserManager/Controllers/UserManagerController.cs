﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Domain;
using ApplicationCore.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace UserManager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserManagerController : ControllerBase
    {
        private IUserService _userServive;

        public UserManagerController(IUserService userServive)
        {
            this._userServive = userServive;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<string>>> Get()
        {
           var result = await _userServive.InsertAsync(new User { Id = "1", Name = "hu" });
            return new string[] { "value1", "value2", result.ToString() };
        }
    }
}