﻿using ApplicationCore.Domain;
using Infrastructure.Repository;
using System.Threading.Tasks;

namespace ApplicationCore.Service
{
    public class UserService : IUserService
    {
        private IRepositoryUser _repositoryUser;
        public UserService(IRepositoryUser repositoryUser)
        {
            _repositoryUser = repositoryUser;
        }

        public async Task<bool> InsertAsync(User user)
        {
            return await _repositoryUser.InsertAsync(user);
              
        }


    }
}
