﻿using ApplicationCore.Domain;
using System.Threading.Tasks;

namespace ApplicationCore.Service
{
    public interface IUserService
    {
        Task<bool> InsertAsync(User user);
    }
}
